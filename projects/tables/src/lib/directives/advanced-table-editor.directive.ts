import {
  ElementRef, Directive, ViewContainerRef, Output, EventEmitter
} from '@angular/core';
import { AdvancedTableComponent } from '../components/advanced-table/advanced-table.component';

export class AdvancedTableNotNullError extends Error {
  constructor() {
    super('Advanced table component cannot be empty at this context.');
  }
}

@Directive(
 {
   selector: '[tableEditor]'
 }
)
export class AdvancedTableEditorDirective {

  public dirty: boolean = false;
  @Output() dirtyChange: EventEmitter<boolean> = new EventEmitter<boolean>(false);

  constructor(private table: AdvancedTableComponent) {
    //
  }

  /**
   * Add rows to table and mark them as added rows
   */
  add(...addItems: any[]) {
    if (this.table == null) {
      throw new AdvancedTableNotNullError();
    }
    // decorate items
    const key = this.table.config.columns[0].property || this.table.config.columns[0].name;
    for (let index = 0; index < addItems.length; index++) {
      const element = addItems[index];
      let tr = this.table.dataTable.rows(':has(input[data-id="' + element[key] + '"])').nodes().to$();
      if (tr.hasClass('removed')) {
        tr.toggleClass('removed text-gray-400 text-decoration-line-through');
      } else {
        this.table.dataTable.rows.add([element]);
        tr = this.table.dataTable.rows(':has(input[data-id="' + element[key] + '"])').nodes().to$();
        tr.toggleClass('added font-weight-bold text-success');
      }
    }
    this.table.dataTable.draw();
    // find removed
    const removedLength = this.table.dataTable.rows('.removed')[0].length;
    // find added
    const addedLength = this.table.dataTable.rows('.added')[0].length;
    // and emit event
    const items = this.table.dataTable.rows().data();
    this.table.load.emit({
      data: items,
      recordsFiltered: items.length - removedLength,
      recordsTotal: items.length - removedLength
    });
    this.dirty = (removedLength > 0 || addedLength > 0);
    this.dirtyChange.emit(this.dirty);
  }

  /**
   * Marks rows as removed rows
   */
  remove(...removeItems: any[]) {
    if (this.table == null) {
      throw new AdvancedTableNotNullError();
    }
    // get key
    const key = this.table.config.columns[0].property || this.table.config.columns[0].name;
    // loop items
      for (let index = 0; index < removeItems.length; index++) {
        const element = removeItems[index];
        const tr = this.table.dataTable.rows(':has(input[data-id="' + element[key] + '"])').nodes().to$();
        // if row is marked as added
        if (tr.hasClass('added')) {
          // remove this dirty row
          this.table.dataTable.rows(tr).remove();
        } else {
          // otherwise unmarked row
          tr.toggleClass('removed text-gray-400 text-decoration-line-through');
        }
        // check if item is selected
        const selectedIndex = this.table.selected.findIndex((item) => {
          return item[key] === element[key];
        });
        // and remove it from selected collection
        if (selectedIndex >= 0) {
          this.table.selected.splice(selectedIndex, 1);
        }
      }
      // re-draw table
      this.table.dataTable.draw();
      // get number of table items
      const items = this.table.dataTable.rows().data();
      // get number of removed items
      const removedLength = this.table.dataTable.rows('.removed')[0].length;
      // get number of added items
      const addedLength = this.table.dataTable.rows('.added')[0].length;
      // refresh
      this.table.load.emit({
        data: items,
        recordsFiltered: items.length - removedLength,
        recordsTotal: items.length - removedLength
      });
      this.dirty = (removedLength > 0 || addedLength > 0);
      this.dirtyChange.emit(this.dirty);
  }

  set(items: any[]) {
    if (this.table == null) {
      throw new AdvancedTableNotNullError();
    }
    // clear rows
    this.table.selected = [];
    this.table.dataTable.clear();
    // set items
    this.table.dataTable.rows.add(items).draw();
    this.table.load.emit({
      data: items,
      recordsFiltered: items.length,
      recordsTotal: items.length
    });
  }

  /**
   * Undo changes
   */
  undo() {
    if (this.table == null) {
      throw new AdvancedTableNotNullError();
    }
    // reset removed rows
    this.table.dataTable.rows('.removed').nodes().to$().toggleClass('removed text-gray-400 text-decoration-line-through');
    // remove added rows
    this.table.dataTable.rows('.added').remove();
    // clear selected items
    this.table.selected = [];
    const items = this.table.dataTable.rows().data();
    this.table.dataTable.draw();
    this.table.load.emit({
      data: items,
      recordsFiltered: items.length,
      recordsTotal: items.length
    });
    this.dirty = false;
    this.dirtyChange.emit(this.dirty);
  }

  rows() {
    if (this.table == null) {
      throw new AdvancedTableNotNullError();
    }
    return Array.from(this.table.dataTable.rows(':not(.removed)').data());
  }

}
